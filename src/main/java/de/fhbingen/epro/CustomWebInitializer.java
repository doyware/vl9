/**
 *
 */
package de.fhbingen.epro;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import de.fhbingen.epro.config.CustomJPARepositoryConfig;
import de.fhbingen.epro.config.CustomRepositoryRestMvcConfiguration;
import de.fhbingen.epro.config.CustomSecurityConfiguration;
import de.fhbingen.epro.cors.CORSFilter;

/**
 *
 * @author Johannes Hiemer.
 *
 */
public class CustomWebInitializer implements WebApplicationInitializer {

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		AnnotationConfigWebApplicationContext rootCtx = new AnnotationConfigWebApplicationContext();
		rootCtx.register(
				CustomJPARepositoryConfig.class,
				CustomSecurityConfiguration.class
				);

		servletContext.addListener(new ContextLoaderListener(rootCtx));

		servletContext.addFilter("corsFilter", CORSFilter.class);
		servletContext.getFilterRegistration("corsFilter").addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST),
				false, "/*");
		
		servletContext.addFilter("openEntityManagerInView", new OpenEntityManagerInViewFilter());
		servletContext.getFilterRegistration("openEntityManagerInView").addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST),
				false, "/*");

		AnnotationConfigWebApplicationContext webCtx = new AnnotationConfigWebApplicationContext();
		webCtx.setParent(rootCtx);
		webCtx.setServletContext(servletContext);
		webCtx.register(CustomRepositoryRestMvcConfiguration.class);

		DispatcherServlet dispatcherServlet = new DispatcherServlet(webCtx);
		ServletRegistration.Dynamic appServlet = servletContext.addServlet("exporter", dispatcherServlet);
		appServlet.setAsyncSupported(true);
		appServlet.setLoadOnStartup(1);
		appServlet.addMapping("/");
	}

}
